$("#inputGroupFile01").change(function (event) {
    RecurFadeIn();
    readURL(this);
});
$("#inputGroupFile01").on('click', function (event) {
    RecurFadeIn();
});

function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        let filename = $("#inputGroupFile01").val();
        filename = filename.substring(filename.lastIndexOf('\\') + 1);
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
            $('#blah').hide();
            $('#blah').fadeIn(500);
            $('.custom-file-label').text(filename);
        };
        reader.readAsDataURL(input.files[0]);
    }
    $(".alert").removeClass("loading").hide();

    $("#submitButton").show();
}

function RecurFadeIn() {
    console.log('ran');
    FadeInAlert();
}

function FadeInAlert() {
    $(".alert").show();
    $(".alert").addClass("loading");
}