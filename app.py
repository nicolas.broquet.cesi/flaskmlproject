from __future__ import absolute_import, division, print_function, unicode_literals

from flask import Flask, render_template, request
from werkzeug import secure_filename
from pymongo import MongoClient
from datetime import datetime

# TensorFlow and tf.keras
import tensorflow as tf
import operator
import numpy as np

AUTOTUNE = tf.data.experimental.AUTOTUNE
client = MongoClient('mongodb://localhost:27017/')
# data base name : 'flaskmlproject'
db = client['flaskmlproject']
app = Flask(__name__)


def calc_flowers_result(file_name):
    input_height = 299
    input_width = 299
    input_mean = 0
    input_std = 255

    input_layer = "Placeholder"
    output_layer = "final_result"
    graph = load_graph("model/output_graph_flowers.pb")
    t = read_tensor_from_image_file(
        file_name,
        input_height=input_height,
        input_width=input_width,
        input_mean=input_mean,
        input_std=input_std)

    input_name = "import/" + input_layer
    output_name = "import/" + output_layer
    input_operation = graph.get_operation_by_name(input_name)
    output_operation = graph.get_operation_by_name(output_name)

    with tf.Session(graph=graph) as sess:
        results = sess.run(output_operation.outputs[0], {
            input_operation.outputs[0]: t
        })
    results = np.squeeze(results)

    top_k = results.argsort()[-5:][::-1]
    labels = load_labels("model/output_labels_flowers.txt")
    current_result = dict()
    for i in top_k:
        current_result[labels[i]] = float(results[i])
    return current_result


def calc_animals_result(file_name):
    input_height = 299
    input_width = 299
    input_mean = 0
    input_std = 255

    input_layer = "Placeholder"
    output_layer = "final_result"
    graph = load_graph("model/output_graph_animals.pb")
    t = read_tensor_from_image_file(
        file_name,
        input_height=input_height,
        input_width=input_width,
        input_mean=input_mean,
        input_std=input_std)

    input_name = "import/" + input_layer
    output_name = "import/" + output_layer
    input_operation = graph.get_operation_by_name(input_name)
    output_operation = graph.get_operation_by_name(output_name)

    with tf.Session(graph=graph) as sess:
        results = sess.run(output_operation.outputs[0], {
            input_operation.outputs[0]: t
        })
    results = np.squeeze(results)

    top_k = results.argsort()[-10:][::-1]
    labels = load_labels("model/output_labels_animals.txt")
    current_result = dict()
    for i in top_k:
        current_result[labels[i]] = float(results[i])
    return current_result


def calc_animals_flowers_result(file_name):
    input_height = 299
    input_width = 299
    input_mean = 0
    input_std = 255

    input_layer = "Placeholder"
    output_layer = "final_result"
    graph = load_graph("model/output_graph_animals_flowers.pb")
    t = read_tensor_from_image_file(
        file_name,
        input_height=input_height,
        input_width=input_width,
        input_mean=input_mean,
        input_std=input_std)

    input_name = "import/" + input_layer
    output_name = "import/" + output_layer
    input_operation = graph.get_operation_by_name(input_name)
    output_operation = graph.get_operation_by_name(output_name)

    with tf.Session(graph=graph) as sess:
        results = sess.run(output_operation.outputs[0], {
            input_operation.outputs[0]: t
        })
    results = np.squeeze(results)

    top_k = results.argsort()[-15:][::-1]
    labels = load_labels("model/output_labels_animals_flowers.txt")
    current_result = dict()
    for i in top_k:
        current_result[labels[i]] = float(results[i])
    return current_result


def load_graph(model_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()

    with open(model_file, "rb") as f:
        graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)

    return graph


def read_tensor_from_image_file(file_name,
                                input_height=299,
                                input_width=299,
                                input_mean=0,
                                input_std=255):
    input_name = "file_reader"
    output_name = "normalized"
    file_reader = tf.read_file(file_name, input_name)
    if file_name.endswith(".png"):
        image_reader = tf.image.decode_png(
            file_reader, channels=3, name="png_reader")
    elif file_name.endswith(".gif"):
        image_reader = tf.squeeze(
            tf.image.decode_gif(file_reader, name="gif_reader"))
    elif file_name.endswith(".bmp"):
        image_reader = tf.image.decode_bmp(file_reader, name="bmp_reader")
    else:
        image_reader = tf.image.decode_jpeg(
            file_reader, channels=3, name="jpeg_reader")
    float_caster = tf.cast(image_reader, tf.float32)
    dims_expander = tf.expand_dims(float_caster, 0)
    resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
    normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])
    sess = tf.Session()
    result = sess.run(normalized)

    return result


def load_labels(label_file):
    label = []
    proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
    for l in proto_as_ascii_lines:
        label.append(l.rstrip())
    return label


@app.route('/')
def hello_world():
    return render_template('upload.html')


def save_result(dict):
    db.results.insert_one(dict)


@app.route('/result', methods=['POST', 'GET'])
def result():
    if request.method == 'POST':
        f = request.files['file']
        file_name_result = 'tmpUploaded/' + secure_filename(f.filename)
        f.save(file_name_result)
        res = dict()
        # test for flowers
        res['animals_flowers'] = dict()
        res['animals_flowers']['results'] = calc_animals_flowers_result(file_name_result)

        max_label = max(res['animals_flowers']['results'].items(), key=operator.itemgetter(1))[0]
        res['max_label'] = dict()
        res['max_label']['result'] = max_label
        res['max_value'] = dict()
        res['max_value']['result'] = res['animals_flowers']['results'][max_label]

        res['date'] = datetime.now()
        res['filename'] = file_name_result
        save_result(res)
        return render_template('result.html', all_results=res)


if __name__ == '__main__':
    app.run()
